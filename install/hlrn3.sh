#!/bin/bash
#PBS -N QE
#PBS -l nodes=1:ppn=24
#PBS -j oe

module switch PrgEnv-cray PrgEnv-intel
module load fftw

export CRAYPE_LINK_TYPE=dynamic

./configure MPIF90=ftn F90=ifort F77=ftn CC=cc --with-scalapack=intel
# F90=ftn does not work for qe-6.0 (a.out not produced)

make pw ph pp
make --ignore-errors epw
