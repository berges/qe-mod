#!/bin/bash
#PBS -N QE
#PBS -l nodes=1:ppn=20
#PBS -j oe
#PBS -q express

# interactively:
# qsub -I -l nodes=1:ppn=20 -q express

module load intel/2016
module load openmpi/2.0

cd $PBS_O_WORKDIR

./configure

make pw pp ph epw
