#!/bin/bash
#SBATCH --nodes 1
#SBATCH --tasks-per-node 96
#SBATCH --partition standard96:test
#SBATCH --time 01:00:00

# Start interactive job:
# srun -N 1 --tasks-per-node 96 -p standard96:test -t 01:00:00 --pty bash

# Download Wannier90 elsewhere and upload it to HLRN:
# wget https://codeload.github.com/wannier-developers/wannier90/tar.gz/v3.1.0
# rsync -av v3.1.0 blogin:/path/to/q-e/archive

# install QE at HLRN-IV "Lise" in Berlin:

module load intel
module load impi

FLAGS="-xCORE-AVX512 -qopt-zmm-usage=high -D__ALPHA"

cd $SLURM_SUBMIT_DIR

./configure \
    MPIF90=mpiifort \
    F90=ifort \
    F77=ifort \
    CC=icc \
    FFLAGS="$FLAGS" \
    CFLAGS="$FLAGS" \
    --with-scalapack=intel

make -j 96 pw pp ph epw
